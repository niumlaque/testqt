#ifndef SIMPLESERVER_H
#define SIMPLESERVER_H

#include <QObject>
#include <QList>
#include "EventHandler.h"
#include "simpleeventargs.h"

class QTcpServer;
class SimpleClient;

class SimpleServer : public QObject
{
    Q_OBJECT
public:
    explicit SimpleServer(QObject *parent = 0);
    virtual ~SimpleServer();

public:
    EventHandler<const ClientEventArgs &> clientConnected;
    EventHandler<const ClientEventArgs &> clientDisconnected;
    EventHandler<const DataReceivedEventArgs &> dataReceived;

protected slots:
    virtual void onClientConnected();

private:
    static void onClientDisconnected(void *sender, const ClientEventArgs &e);
    static void onClientDataReceived(void *sender, const DataReceivedEventArgs &e);

public:
    void listen(quint16 port);
    void send(const char *buffer, qint32 length);

private:
    QTcpServer *_server;
    QList<SimpleClient *> _clients;
};

#endif // SIMPLESERVER_H
