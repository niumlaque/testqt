#include "qtquick1applicationviewer.h"
#include <QApplication>
#include <unistd.h>
#include "simpleserver.h"
#include "simpleclient.h"

namespace
{
    QtQuick1ApplicationViewer *g_view;

    void connected(void *sender, const ClientEventArgs &e)
    {
        // e.host().toString() で環境によっては Segv が発生する。。。
        //qDebug("connected from %s:%u", e.host().toString().toLatin1().data(), e.port());
    }

    void disconnected(void *sender, const ClientEventArgs &e)
    {
        //qDebug("disconnected from %s:%u", e.host().toString().toLatin1().data(), e.port());
    }

    void dataReceived(void *sender, const DataReceivedEventArgs &e)
    {
        static unsigned long long id=0;
        static char path[256] = {};

        unlink(path);
        ::sprintf(path, "/tmp/%llu.qml", ++id);

        std::string s(e.buffer(), e.buffer() + e.length());

        FILE *fp = ::fopen(path, "w");
        ::fputs(s.c_str(), fp);
        ::fclose(fp);
        g_view->setMainQmlFile(QLatin1String(path));
    }
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QtQuick1ApplicationViewer viewer;
    viewer.addImportPath(QLatin1String("modules"));
    viewer.setOrientation(QtQuick1ApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/Test/main.qml"));
    viewer.showExpanded();
    g_view = &viewer;

    SimpleServer server;
    server.clientConnected += connected;
    server.clientDisconnected += disconnected;
    server.dataReceived += dataReceived;
    server.listen(6666);

    return app.exec();
}
