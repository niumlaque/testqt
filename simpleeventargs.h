#ifndef SIMPLEEVENTARGS_H
#define SIMPLEEVENTARGS_H

#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

class ClientEventArgs
{
public:
    ClientEventArgs(const QHostAddress &host, quint16 port)
        : _host(host)
        , _port(port)
    {}

    ClientEventArgs(const ClientEventArgs &other)
        : _host(other._host)
        , _port(other._port)
    {}

public:
    const QHostAddress &host() const { return this->_host; }
    quint16 port() const { return this->_port; }

private:
    const QHostAddress &_host;
    const quint16 _port;
};

class DataReceivedEventArgs : public ClientEventArgs
{
public:
    DataReceivedEventArgs(const QHostAddress &host, quint16 port, char *buffer, qint32 length)
        : ClientEventArgs(host, port)
        , _buffer(buffer)
        , _length(length)
    {}

    DataReceivedEventArgs(const ClientEventArgs &clientInfo, char *buffer, qint32 length)
        : ClientEventArgs(clientInfo.host(), clientInfo.port())
        , _buffer(buffer)
        , _length(length)
    {}

    DataReceivedEventArgs(const DataReceivedEventArgs &other)
        : ClientEventArgs(other)
        , _buffer(other._buffer)
        , _length(other._length)
    {}

public:
    const char *buffer() const { return this->_buffer; }
    qint32 length() const { return this->_length; }

private:
    const char *_buffer;
    const qint32 _length;
};

namespace
{
    ClientEventArgs createClientInfo(QTcpSocket *socket)
    {
        Q_ASSERT(socket != NULL);

        const QHostAddress address = socket->peerAddress();
        qint16 port = socket->peerPort();
        return ClientEventArgs(address, port);
    }
}

#endif // SIMPLEEVENTARGS_H
