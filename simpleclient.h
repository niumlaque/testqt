#ifndef SIMPLECLIENT_H
#define SIMPLECLIENT_H

#include <functional>
#include <QObject>
#include "EventHandler.h"
#include "simpleeventargs.h"

class QTcpSocket;
class SimpleServer;

class SimpleClient : public QObject
{
    Q_OBJECT

    friend class SimpleServer;

public:
    explicit SimpleClient(QObject *parent = 0);
    explicit SimpleClient(QTcpSocket *socket, SimpleServer *parent);
    virtual ~SimpleClient();

public:
    EventHandler<const ClientEventArgs &> connected;
    EventHandler<const ClientEventArgs &> disconnected;
    EventHandler<const DataReceivedEventArgs &> dataReceived;

protected slots:
    virtual void onConnected();
    virtual void onDisconnected();
    virtual void onDataReceived();

public:
    bool connectToHost(const QString host, quint16 port);
    bool send(const char *buffer, qint32 length);

private:
    void initialize();

private:
    QTcpSocket *_socket;
    SimpleServer *_parent;
    bool _isSocketOwner;
};

#endif // SIMPLECLIENT_H
