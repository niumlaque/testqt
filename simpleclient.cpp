#include "simpleclient.h"
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

namespace
{
    const qint32 BUFFER_SIZE = 1024;
}

SimpleClient::SimpleClient(QObject *parent)
    : QObject(parent)
    , _parent(NULL)
    , _isSocketOwner(true)
{
    this->_socket = new QTcpSocket(this);
    initialize();
}

SimpleClient::SimpleClient(QTcpSocket *socket, SimpleServer *parent)
    : QObject(socket->parent())
    , _parent(parent)
    , _isSocketOwner(false)
{
    this->_socket = socket;
    initialize();
}

SimpleClient::~SimpleClient()
{
    if(this->_isSocketOwner)
        delete this->_socket;
}

void SimpleClient::initialize()
{
    connect(this->_socket, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(this->_socket, SIGNAL(readyRead()), this, SLOT(onDataReceived()));
    connect(this->_socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

void SimpleClient::onConnected()
{
    this->connected(this, createClientInfo(this->_socket));
}

void SimpleClient::onDisconnected()
{
    this->disconnected(this, createClientInfo(this->_socket));
}

void SimpleClient::onDataReceived()
{
    char buffer[BUFFER_SIZE] = {};
    this->_socket->read(buffer, this->_socket->bytesAvailable());

    const ClientEventArgs clientInfo = createClientInfo(this->_socket);
    DataReceivedEventArgs e(clientInfo, buffer, sizeof(buffer));
    this->dataReceived(this, e);
}

bool SimpleClient::connectToHost(const QString host, quint16 port)
{
    if (this->_socket->state() != QAbstractSocket::UnconnectedState)
        return false;

    this->_socket->connectToHost(host, port);
    return this->_socket->waitForConnected();
}

bool SimpleClient::send(const char *buffer, qint32 length)
{
    if (this->_socket->state() != QAbstractSocket::ConnectedState)
        return false;

    if (length > BUFFER_SIZE)
        return false;

    this->_socket->write(buffer);
    return this->_socket->waitForBytesWritten();
}







