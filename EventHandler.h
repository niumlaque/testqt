#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include <vector>

template<typename T>
class EventHandler
{
private:
    typedef void(*FuncType)(void *, T);
    typedef typename std::vector<FuncType>::iterator iterator;
    typedef typename std::vector<FuncType>::reverse_iterator reverse_iterator;

public:
    EventHandler() { }

public:
    // Overwrite.
    EventHandler<T> &operator =(FuncType func)
    {
        { std::vector<FuncType>().swap(this->_notifylist); }
        (*this) += func;

        return *this;
    }

    // Attach.
    void operator +=(FuncType func)
    {
        this->_notifylist.push_back(func);
    }

    // Detach.
    void operator -=(FuncType func)
    {
        for(reverse_iterator it = this->_notifylist.rbegin(); it != this->_notifylist.rend(); ++it)
        {
            if(*it == func)
            {
                this->_notifylist.erase(it.base() - 1);
                break;
            }
        }
    }

    // Notify.
    void operator ()(void *sender, T &value)
    {
        for(iterator it = this->_notifylist.begin(); it != this->_notifylist.end(); ++it)
            (*it)(sender, value);
    }

private:
    EventHandler(const EventHandler<T> &other) { }

private:
    void operator =(const EventHandler<T> &other) { }

private:
    std::vector<FuncType> _notifylist;
};

template<>
class EventHandler<void>
{
private:
    typedef void(*FuncType)(void *);
    typedef typename std::vector<FuncType>::iterator iterator;
    typedef typename std::vector<FuncType>::reverse_iterator reverse_iterator;

public:
    EventHandler() { }

public:
    // Overwrite.
    EventHandler<void> &operator =(FuncType func)
    {
        { std::vector<FuncType>().swap(this->_notifylist); }
        (*this) += func;

        return *this;
    }

    // Attach.
    void operator +=(FuncType func)
    {
        this->_notifylist.push_back(func);
    }

    // Detach.
    void operator -=(FuncType func)
    {
        for(reverse_iterator it = this->_notifylist.rbegin(); it != this->_notifylist.rend(); ++it)
        {
            if(*it == func)
            {
                this->_notifylist.erase(it.base() - 1);
                break;
            }
        }
    }

    // Notify.
    void operator ()(void *sender)
    {
        for(iterator it = this->_notifylist.begin(); it != this->_notifylist.end(); ++ it)
            (*it)(sender);
    }

private:
    EventHandler(const EventHandler<void> &) { }

private:
    void operator =(const EventHandler<void> &) { }

private:
    std::vector<FuncType> _notifylist;
};

#endif // EVENTHANDLER_H
