#include "simpleserver.h"
#include <QtNetwork/QTcpServer>
#include "simpleclient.h"

SimpleServer::SimpleServer(QObject *parent)
    : QObject(parent)
{
    this->_server = new QTcpServer(this);
    connect(this->_server, SIGNAL(newConnection()), this, SLOT(onClientConnected()));
}

SimpleServer::~SimpleServer()
{
    delete this->_server;
}

void SimpleServer::onClientConnected()
{
    QTcpSocket *socket = this->_server->nextPendingConnection();
    SimpleClient *client = new SimpleClient(socket, this);
    client->disconnected += onClientDisconnected;
    client->dataReceived += onClientDataReceived;
    this->_clients.append(client);

    const ClientEventArgs clientInfo = createClientInfo(socket);
    this->clientConnected(this, clientInfo);
}

void SimpleServer::onClientDisconnected(void *sender, const ClientEventArgs &e)
{
    //SimpleClient *client = dynamic_cast<SimpleClient *>(sender());
    SimpleClient *client = static_cast<SimpleClient *>(sender);
    SimpleServer *server = client->_parent;

    server->_clients.removeAll(client);
    server->clientDisconnected(server, e);
}

void SimpleServer::onClientDataReceived(void *sender, const DataReceivedEventArgs &e)
{
    //SimpleClient *client = dynamic_cast<SimpleClient *>(sender());
    SimpleClient *client = static_cast<SimpleClient *>(sender);
    SimpleServer *server = client->_parent;

    server->dataReceived(server, e);
}

void SimpleServer::listen(quint16 port)
{
    this->_server->listen(QHostAddress::Any, port);
}

void SimpleServer::send(const char *buffer, qint32 length)
{
    for(QList<SimpleClient *>::iterator it = this->_clients.begin(); it != this->_clients.end(); ++it)
        (*it)->send(buffer, length);
}
